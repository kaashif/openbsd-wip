OpenBSD Ports
=============
In this repo, you'll find some ports I had lying around
/usr/ports/mystuff which I might not have submitted to ports@ (not
good enough yet) or did not get committed. In any case, I try to keep
these ports up-to-date while I try to get them committed.

Generally, I only bother to test on amd64, since that's where I use
these programs. I try to get them to build on sparc64 too, but I can't
test any of the graphical programs there.

All of the Makefiles, patches, and things I actually wrote are under
the 2-clause BSD license you can find in the LICENSE file. All of the
code that is fetched by the Makefiles is under whichever license the
authors chose.
